(ns jobtech-taxonomy-api-gitops.database
  (:require [wanderung.core :as wanderung]
            [wanderung.datom :as datom]
            [clojure.string :as str]
            [datomic.client.api :as datomic]
            [clojure.spec.alpha :as s]
            [cheshire.core :as cheshire]
            [clojure.java.shell :as shell]
            [clojure.java.io :as io]
            [jobtech-datomic.proxy :as datomic-proxy]
            [clojure.pprint :as pp])
  (:import [java.text SimpleDateFormat]
           [java.util TimeZone Date]))

(defn- local-date-format-from-string [src]
  (doto (SimpleDateFormat. src)
    (.setTimeZone (TimeZone/getTimeZone "Europe/Berlin"))))

(def date-format (local-date-format-from-string "yyyy-MM-dd-HH-mm-ss"))

(defn get-timestamp []
  (.format date-format (Date.)))

(defn sh-out
  "Perform a shell command and return the standard output or throw an exception if the return code is different from zero."
  [& args]
  (let [result (apply shell/sh args)]
    (if (= 0 (:exit result))
      (:out result)
      (throw (ex-info "Failed to run shell command"
                      (assoc result :shell-args args))))))

(def backends-cfg-filename "Dhall/Backends.dhall")

(defn conform-validate
  "Conform something with a spec or throw an exception with an explanation if it does not conform."
  [sp x]
  (let [c (s/conform sp x)]
    (if (= c ::s/invalid)
      (throw (ex-info (str "Validation failed: " (s/explain-str sp x))
                      {:spec sp
                       :value x}))
      c)))

(s/def :backends-cfg/DATAHIKE_CFG__STORE__BACKEND #{":file"})
(s/def :backends-cfg/DATAHIKE_CFG__STORE__DBTYPE string?)
(s/def :backends-cfg/DATAHIKE_CFG__STORE__PATH string?)

(s/def :backends-cfg/DATOMIC_CFG__ENDPOINT string?)
(s/def :backends-cfg/DATOMIC_CFG__REGION string?)
(s/def :backends-cfg/DATOMIC_CFG__SYSTEM string?)
(s/def :backends-cfg/datomic_name string?)

(s/def :backends-cfg/datomic-config
  (s/keys :req-un
          [:backends-cfg/DATOMIC_CFG__ENDPOINT
           :backends-cfg/DATOMIC_CFG__REGION
           :backends-cfg/DATOMIC_CFG__SYSTEM
           :backends-cfg/DATOMIC_CFG__DATOMIC_NAME]))
(s/def :backends-cfg/datahike-config
  (s/keys :req-un
          [:backends-cfg/DATAHIKE_CFG__STORE__BACKEND
           :backends-cfg/DATAHIKE_CFG__STORE__DBTYPE
           :backends-cfg/DATAHIKE_CFG__STORE__PATH]))

(s/def :backends-cfg/config
  (s/or :datomic :backends-cfg/datomic-config
        :datahike :backends-cfg/datahike-config))

(defn- with-proxy-if-datomic [cfg f]
  (if (= :datomic (:wanderung/type cfg))
    (do
      (println "Starting datomic proxy to" (:system cfg))
      (with-open [proxy (datomic-proxy/start! (select-keys cfg [:region :system]))]
        (println "Started proxy on port" (:port proxy))
        (f (assoc cfg :proxy-port (:port proxy)))))
    (f cfg)))

(s/fdef with-proxies-if-datomics :args (s/cat :cfg-syms (s/* simple-symbol?) :expr any?))
(defmacro with-proxies-if-datomics [& cfg-syms+expr]
  (let [[syms expr] ((juxt butlast last) cfg-syms+expr)]
    (reduce (fn [acc sym]
              `(with-proxy-if-datomic ~sym (fn [~sym] ~acc))) expr syms)))

(defn- wrap-with-datomic-proxies [copy-fn]
  (fn [src-cfg dst-cfg]
    (with-proxies-if-datomics src-cfg dst-cfg (copy-fn src-cfg dst-cfg))))

(defn- wrap-ensure-src-exists [copy-fn]
  (fn [src-cfg dst-cfg]
    (case (:wanderung/type src-cfg)
      :datomic
      (let [dbs (-> src-cfg datomic/client (datomic/list-databases {:limit -1}))]
        (when-not (some #{(:name src-cfg)} dbs)
          (throw (ex-info "Source database does not exist" {:src-cfg src-cfg
                                                            :dbs dbs}))))
      :nippy
      (when-not (.exists (io/file (:filename src-cfg)))
        (throw (ex-info "Source db file does not exist" src-cfg)))

      nil)
    (println "Source database exists")
    (copy-fn src-cfg dst-cfg)))

(defn- wrap-create-dst-database [copy-fn]
  (fn [src-cfg dst-cfg]
    (case (:wanderung/type dst-cfg)
      :datomic
      (do (datomic/create-database (datomic/client dst-cfg)
                                   {:db-name (:name dst-cfg)})
          (Thread/sleep 5000) ;; db is not immediately available
          (println "Created" (:name dst-cfg)))

      :nippy
      (do (io/make-parents (:filename dst-cfg))
          (println "Created parents for" (:filename dst-cfg)))

      nil)
    (copy-fn src-cfg dst-cfg)))

(defn- wrapping-use-new-database-name-if-dst-is-backend-config [{:keys [dbname-decorator]}]
  (fn [copy-fn]
    (fn [src-cfg dst-cfg]
      (if (:config-name dst-cfg)
        (let [old-db-name (:name dst-cfg)
              re #"^jobtech-taxonomy-(.+)-\d{4}.+$"
              db-type-old (or (second (re-matches re old-db-name))
                              (throw (ex-info "Can't extract db type from its name"
                                              {:name old-db-name :re re})))
              db-type (if (seq dbname-decorator) dbname-decorator db-type-old)
              new-db-name (str "jobtech-taxonomy-" db-type "-" (get-timestamp))
              ret (copy-fn src-cfg (assoc dst-cfg :name new-db-name))
              old-backends-content (slurp backends-cfg-filename)
              new-backends-content (str/replace old-backends-content old-db-name new-db-name)]
          (if (= old-backends-content new-backends-content)
            (println "No changes to file" backends-cfg-filename)
            (do (spit backends-cfg-filename new-backends-content)
                (println "Patched file" backends-cfg-filename)))
          ret)
        (copy-fn src-cfg dst-cfg)))))

(defn- wrap-sanity-check [copy-fn]
  (fn [src-cfg dst-cfg]
    (let [ret (copy-fn src-cfg dst-cfg)]
      (println "Sanity check...")
      (println
        (if (datom/similar-datoms? (distinct (wanderung/datoms-from-storage src-cfg))
                                   (distinct (wanderung/datoms-from-storage dst-cfg)))
          "SUCCESSFULLY copied datoms"
          "FAILED to copy datoms"))
      ret)))

(defn- wanderung-config
  "Produce a wanderung database config from a raw config from the api-gitops repo"
  [raw-config]
  (let [[cfg-type _] (conform-validate :backends-cfg/config raw-config)]
    (case cfg-type
      :datomic {:wanderung/type :datomic
                :name (:DATOMIC_CFG__DATOMIC_NAME raw-config)
                :server-type :ion
                :region (:DATOMIC_CFG__REGION raw-config)
                :system (:DATOMIC_CFG__SYSTEM raw-config)
                :endpoint (:DATOMIC_CFG__ENDPOINT raw-config)}
      ;; TODO implement
      :datahike {:wanderung/type :datahike})))

(defn- evaluate-configs
  "Load the all the database configurations from the local gitops repo"
  []
  (let [raw-configs (for [cluster ["Test" "Prod"]]
                      (cheshire/parse-string
                        (sh-out "dhall-to-json"
                                "--file"
                                backends-cfg-filename
                                :env (assoc (into {} (System/getenv))
                                       "CLUSTER" (str "<Test|Prod>." cluster)))
                        keyword))]
    (into {}
          (comp cat
                (filter #(-> % :data :DATOMIC_CFG__SYSTEM))
                (map (fn [item]
                       (let [name (-> item :metadata :name keyword)]
                         [name (-> item
                                   :data
                                   wanderung-config
                                   (assoc :config-name name))]))))
          raw-configs)))

(defn- wrapping-disp-config [message]
  (fn [copy-fn]
    (fn [src dst]
      (println message)
      (println "Src:")
      (pp/pprint src)
      (println "Dst:")
      (pp/pprint dst)
      (copy-fn src dst))))

(def wrap-disp-config-after-coercion (wrapping-disp-config "*** Configs after coercion"))

(defn- wrap-coerce-to-config [copy-fn]
  (fn [src dst]
    (let [configs (delay (evaluate-configs))
          ->cfg (fn [x]
                  (cond
                    (string? x)
                    {:wanderung/type :nippy
                     :filename x}

                    (keyword? x)
                    (or (get @configs x)
                        (throw (ex-info "Missing configuration"
                                        {:key x
                                         :available-keys (keys src)})))

                    (:wanderung/type x)
                    x

                    :else
                    (throw (ex-info "Unknown configuration" {:value x}))))
          src-cfg (->cfg src)
          dst-cfg (->cfg dst)]
      (copy-fn src-cfg dst-cfg))))

(defn copy-impl [src dst opts]
  ;; Using comp instead of -> so reading order matches execution order
  (((comp
     wrap-coerce-to-config
     wrap-disp-config-after-coercion
     wrap-with-datomic-proxies
     wrap-ensure-src-exists
     (wrapping-use-new-database-name-if-dst-is-backend-config opts)
     wrap-create-dst-database
     wrap-sanity-check
     (wrapping-disp-config "*** Final configs"))
    wanderung/migrate) src dst))

(defn rewrite-history-impl [transform-datoms src dst opts]
  (let [f ((comp
             wrap-coerce-to-config
             wrap-disp-config-after-coercion
             wrap-with-datomic-proxies
             wrap-ensure-src-exists
             (wrapping-use-new-database-name-if-dst-is-backend-config opts)
             wrap-create-dst-database)
           (fn [src-cfg dst-cfg]
             (wanderung/datoms-to-storage
               dst-cfg
               (transform-datoms
                 (wanderung/datoms-from-storage src-cfg)))))]
    (f src dst)))

(defn copy
  "Copy database from :src to :dst

  src and dst can be:
  - string - file path
  - keyword - configName of a backend defined in Backends.dhall
  - map - wanderung config map

  When dst is a configName, copy will pick a new name and update Backends.dhall
  accordingly"
  [{:keys [src dst] :as opts}]
  (println "Copying from" src "to" dst)
  (copy-impl src dst opts)
  (println "Done copying from" src "to" dst))

(defn rewrite-history
  "Get datoms of :src, modify using :fn, and save into :dst

  src and dst can be:
  - string - file path
  - keyword - configName of a backend defined in Backends.dhall
  - map - wanderung config map

  fn is either:
  - a function object (when used from code)
  - qualified symbol identifying the function (when used from `clj -X ...`)
  - unqualified symbol identifying ns with
    `jobtech-taxonomy-api-gitops.rewrite-history.` prefix that should define
    `transform` function (when used from `clj -X ...`)

  When dst is a configName, rewrite process will pick a new name and update
  Backends.dhall accordingly"
  [{:keys [src dst fn] :as opts}]
  {:pre [src dst fn]}
  (println "Rewriting history from" src "to" dst "using" fn)
  (rewrite-history-impl
   (cond
     (simple-symbol? fn)
     (requiring-resolve
      (symbol (str "jobtech-taxonomy-api-gitops.rewrite-history." (name fn))
              "transform"))

     (qualified-symbol? fn)
     (requiring-resolve fn)

     :else fn)
   src
   dst
   opts)
  (println "Done rewriting history"))

(defn output-configs
  "Writes all database configurations to the standard output"
  [{:keys []}]
  (clojure.pprint/pprint (evaluate-configs)))

(comment

  ;; Download the prod-write database
  (future
    (copy {:src :datomic-prod-editor-config
           :dst "build/prod_write.nippy"}))

  ;; rewrite
  (future
    (rewrite-history
      {:src "build/prod_write.nippy"
       :dst "build/prod_rewrite.nippy"
       :fn 'add-concept-types}))

  ;; Publish a new test database
  (future
    (copy {:src "build/prod_rewrite.nippy"
           :dst :datomic-test-config}))

  (future
    (copy {:src "build/prod_rewrite.nippy"
           :dst :datomic-test-frontend-config}))

  ;; prod

  (future
    (copy {:src "build/prod_rewrite.nippy"
           :dst :datomic-prod-config}))
  (future
    (copy {:src "build/prod_rewrite.nippy"
           :dst :datomic-prod-editor-config}))

  ,)
