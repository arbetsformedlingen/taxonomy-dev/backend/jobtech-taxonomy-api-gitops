{-
  Import Kubernetes bindings only in one place
-}

--| Import Kubernetes 1.19 bindings
let Kubernetes =
      https://raw.githubusercontent.com/dhall-lang/dhall-kubernetes/299351e9df605a149143e029287ebb0ad340c218/1.19/package.dhall
        sha256:78f4f1b2ad7c95f8724f898f51243b21b6623ea96d5ca21992d1598913057cc6

in  Kubernetes
