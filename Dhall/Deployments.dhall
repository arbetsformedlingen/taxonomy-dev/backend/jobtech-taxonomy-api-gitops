{-
  Generates deployments for the API and Varnish servers
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let map = Prelude.List.map

let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let envs = ./Envs.dhall

let utils = ./Utils.dhall

let livenessEp = "/v1/taxonomy/status/health"

let readinessEp = "/v1/taxonomy/status/ready"

let resources
    : Text -> Text -> Text -> Text -> kube.ResourceRequirements.Type
    = \(cpuLimit : Text) ->
      \(memoryLimit : Text) ->
      \(cpuRequest : Text) ->
      \(memoryRequest : Text) ->
        kube.ResourceRequirements::{
        , limits = Some (toMap { cpu = cpuLimit, memory = memoryLimit })
        , requests = Some (toMap { cpu = cpuRequest, memory = memoryRequest })
        }

let datomicApiResources
    : kube.ResourceRequirements.Type
    = resources "0.2" "7Gi" "0.1" "4Gi"

let varnishResources
    : kube.ResourceRequirements.Type
    = resources "0.4" "400Mi" "0.1" "200Mi"

--| Create a `kube.Probe.Type` object from a `port` and a `period` (seconds)
let probe
    : Natural -> Natural -> Text -> Text -> kube.Probe.Type
    = \(port : Natural) ->
      \(period : Natural) ->
      \(ep : Text) ->
      \(key : Text) ->
        kube.Probe::{
        , failureThreshold = Some 3
        , httpGet = Some kube.HTTPGetAction::{
          , httpHeaders = Some
            [ kube.HTTPHeader::{ name = "api-key", value = key } ]
          , path = Some ep
          , port = kube.NatOrString.Nat port
          }
        , initialDelaySeconds = Some 30
        , periodSeconds = Some period
        , successThreshold = Some 1
        , timeoutSeconds = Some 30
        }

--| Create a `kube.Affinity.Type` object from an `app` name
let affinity
    : Text -> Text -> kube.Affinity.Type
    = \(app : Text) ->
      \(name : Text) ->
        kube.Affinity::{
        , podAntiAffinity = Some kube.PodAntiAffinity::{
          , preferredDuringSchedulingIgnoredDuringExecution = Some
            [ kube.WeightedPodAffinityTerm::{
              , podAffinityTerm = kube.PodAffinityTerm::{
                , labelSelector = Some kube.LabelSelector::{
                  , matchLabels = utils.labels app name
                  }
                , topologyKey = "kubernetes.io/hostname"
                }
              , weight = 1
              }
            ]
          }
        }

--| Create a basic deployment object from an `app`, `name` and a list of `containers`
let deployment
    : Text ->
      Text ->
      Natural ->
      List kube.Container.Type ->
      Optional (List kube.Volume.Type) ->
        kube.Deployment.Type
    = \(app : Text) ->
      \(name : Text) ->
      \(replicas : Natural) ->
      \(containers : List kube.Container.Type) ->
      \(volumes : Optional (List kube.Volume.Type)) ->
        let labels
            : Optional (List { mapKey : Text, mapValue : Text })
            = utils.labels app name

        in  kube.Deployment::{
            , metadata = kube.ObjectMeta::{ name = Some name, labels }
            , spec = Some kube.DeploymentSpec::{
              , replicas = Some replicas
              , selector = kube.LabelSelector::{ matchLabels = labels }
              , template = kube.PodTemplateSpec::{
                , metadata = Some kube.ObjectMeta::{ labels }
                , spec = Some kube.PodSpec::{
                  , affinity = Some (affinity app name)
                  , containers
                  , volumes
                  }
                }
              }
            }

--| Create an API deployment object from an environment `EnvConfig`
let deploymentApi
    : cfg.EnvConfig -> kube.Deployment.Type
    = \(c : cfg.EnvConfig) ->
        let name
            : Text
            = utils.apiName c.name

        let port
            : Natural
            = cfg.apiPort

        let containers
            : List kube.Container.Type
            = [ kube.Container::{
                , envFrom = Some
                  [ kube.EnvFromSource::{
                    , secretRef = Some kube.SecretEnvSource::{
                      , name = Some "jobtech-taxonomy-api-secrets"
                      }
                    }
                  , kube.EnvFromSource::{
                    , configMapRef = Some kube.ConfigMapEnvSource::{
                      , name = Some c.dbConfigName
                      }
                    }
                  , kube.EnvFromSource::{
                    , configMapRef = Some kube.ConfigMapEnvSource::{
                      , name = Some "api-java-options-heap"
                      }
                    }
                  ]
                , name
                , image = Some "${cfg.apiImageUrl}:${c.imageApi}"
                , imagePullPolicy = Some "Always"
                , volumeMounts =
                    if    c.volumeMount
                    then  Some
                            [ kube.VolumeMount::{
                              , mountPath = "/mnt/datahike"
                              , name = "database"
                              }
                            ]
                    else  None (List kube.VolumeMount.Type)
                , ports = Some
                  [ kube.ContainerPort::{
                    , containerPort = port
                    , protocol = Some "TCP"
                    }
                  ]
                , resources =
                    if    c.useResourceLimits
                    then  Some datomicApiResources
                    else  None (kube.ResourceRequirements.Type)
                , livenessProbe = Some (probe port 30 livenessEp "liveness")
                , readinessProbe = Some (probe port 30 readinessEp "readiness")
                }
              ]

        let volumes
            : Optional (List kube.Volume.Type)
            = if    c.volumeMount
              then  Some
                      [ kube.Volume::{
                        , name = "database"
                        , persistentVolumeClaim = Some kube.PersistentVolumeClaimVolumeSource::{
                          , claimName = "datahike-file-db-claim"
                          }
                        }
                      ]
              else  None (List kube.Volume.Type)

        in  deployment
              (utils.appName c.name)
              name
              c.nbrApiPods
              containers
              volumes

--| Create a Varnish deployment object from an environment `EnvConfig`
let deploymentVarnish
    : cfg.EnvConfig -> kube.Deployment.Type
    = \(c : cfg.EnvConfig) ->
        let name
            : Text
            = utils.varnishName c.name

        let port
            : Natural
            = cfg.varnishPort

        let containers
            : List kube.Container.Type
            = [ kube.Container::{
                , env = Some
                  [ kube.EnvVar::{
                    , name = "TAXONOMYSERVICE"
                    , value = Some (utils.apiUrl c.name)
                    }
                  , kube.EnvVar::{
                    , name = "TAXONOMYSERVICEPORT"
                    , value = Some (Natural/show cfg.apiPort)
                    }
                  ]
                , name
                , image = Some "${cfg.varnishImageUrl}:${c.imageVarnish}"
                , imagePullPolicy = Some "Always"
                , ports = Some
                  [ kube.ContainerPort::{
                    , containerPort = port
                    , protocol = Some "TCP"
                    }
                  ]
                , resources = Some varnishResources
                , livenessProbe = Some (probe port 30 livenessEp "liveness")
                , readinessProbe = Some (probe port 10 readinessEp "readiness")
                }
              ]

        in  deployment
              (utils.appName c.name)
              name
              c.nbrVarnishPods
              containers
              (None (List kube.Volume.Type))

in    map cfg.EnvConfig kube.Deployment.Type deploymentApi envs
    # map cfg.EnvConfig kube.Deployment.Type deploymentVarnish envs
