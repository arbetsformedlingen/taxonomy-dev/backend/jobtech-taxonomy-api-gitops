{-
  Generates services for the API and Varnish servers
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let map = Prelude.List.map

let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let envs = ./Envs.dhall

let utils = ./Utils.dhall

--| Create a basic service object from an `app`, `name` and a `port`
let service
    : Text -> Text -> Natural -> kube.Service.Type
    = \(app : Text) ->
      \(name : Text) ->
      \(port : Natural) ->
        kube.Service::{
        , metadata = kube.ObjectMeta::{
          , name = Some name
          , labels = utils.labels app name
          }
        , spec = Some kube.ServiceSpec::{
          , selector = utils.labels app name
          , type = Some "NodePort"
          , ports = Some
            [ kube.ServicePort::{
              , name = Some "${Natural/show port}-tcp"
              , targetPort = Some (kube.NatOrString.Nat port)
              , port
              , protocol = Some "TCP"
              }
            ]
          }
        }

--| Create an API service object from an environment `Config`
let serviceApi
    : cfg.EnvConfig -> kube.Service.Type
    = \(c : cfg.EnvConfig) ->
        service (utils.appName c.name) (utils.apiName c.name) cfg.apiPort

--| Create a Varnish service object from an environment `Config`
let serviceVarnish
    : cfg.EnvConfig -> kube.Service.Type
    = \(c : cfg.EnvConfig) ->
        service
          (utils.appName c.name)
          (utils.varnishName c.name)
          cfg.varnishPort

in    map cfg.EnvConfig kube.Service.Type serviceApi envs
    # map cfg.EnvConfig kube.Service.Type serviceVarnish envs
