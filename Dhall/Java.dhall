{-
  Configure the Java resources for deployments
-}
let kube = ./Kubernetes.dhall

let createJavaOptions
    : Text -> Text -> Text -> kube.ConfigMap.Type
    = \(configName : Text) ->
      \(xms : Text) ->
      \(xmx : Text) ->
        kube.ConfigMap::{
        , metadata = kube.ObjectMeta::{ name = Some configName }
        , data = Some
            ( toMap
                { JDK_JAVA_OPTIONS =
                    "-Xmx${xmx} -Xms${xms}"
                }
            )
        }

let apiJavaOptions
    : kube.ConfigMap.Type
    = createJavaOptions "api-java-options-heap" "3G" "7G"

in  [ apiJavaOptions ]
