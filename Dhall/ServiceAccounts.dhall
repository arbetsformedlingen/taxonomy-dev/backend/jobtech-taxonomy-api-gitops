{-
  Generates ServiceAccounts
-}

--| Import the Prelude
let Prelude = ./Prelude.dhall

let map = Prelude.List.map

let kube = ./Kubernetes.dhall

let cfg = ./Config.dhall

let nexusSecret
    : Text
    = "nexus-secret"

--| Create a ServiceAccount
let serviceAccount
    : Text -> kube.ServiceAccount.Type
    = \(name : Text) ->
        kube.ServiceAccount::{
        , metadata = kube.ObjectMeta::{ name = Some name }
        , imagePullSecrets = Some
          [ kube.LocalObjectReference::{ name = Some nexusSecret } ]
        , secrets = Some
          [ kube.ObjectReference::{
            , name = Some nexusSecret
            , apiVersion = "v1"
            , kind = "kubernetes.io/dockerconfigjson"
            }
          ]
        }

let accounts
    : List Text
    = merge
        { Test = [ "default", "deployer", "pipeline" ]
        , Prod = [ "default", "deployer" ]
        }
        cfg.cluster

in  map Text kube.ServiceAccount.Type serviceAccount accounts
